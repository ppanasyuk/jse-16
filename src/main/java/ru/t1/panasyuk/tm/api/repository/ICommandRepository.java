package ru.t1.panasyuk.tm.api.repository;

import ru.t1.panasyuk.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}