package ru.t1.panasyuk.tm.exception.field;

public class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project id is empty...");
    }

}