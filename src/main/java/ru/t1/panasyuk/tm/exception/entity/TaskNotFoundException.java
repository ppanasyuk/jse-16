package ru.t1.panasyuk.tm.exception.entity;

public class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}