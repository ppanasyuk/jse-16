package ru.t1.panasyuk.tm.exception.field;

public class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}