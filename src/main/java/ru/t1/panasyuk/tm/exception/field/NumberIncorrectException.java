package ru.t1.panasyuk.tm.exception.field;

public class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value) {
        super("Error! This value \"" + value + "\" is incorrect...");
    }

}